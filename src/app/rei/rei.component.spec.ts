import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReiComponent } from './rei.component';

describe('ReiComponent', () => {
  let component: ReiComponent;
  let fixture: ComponentFixture<ReiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
